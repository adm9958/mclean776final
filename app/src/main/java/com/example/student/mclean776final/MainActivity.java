package com.example.student.mclean776final;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

public class MainActivity extends AppCompatActivity
        implements CalculatorFragment.OnFragmentInteractionListener,
        AboutFragment.OnFragmentInteractionListener {

    private CalculatorFragment calculatorFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calculatorFragment = new CalculatorFragment();
        getFragmentManager().beginTransaction()
                .add(R.id.calculatorFrameLayout, calculatorFragment, "calculatorFragment")
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.aboutItem:
                AboutFragment aboutFragment = new AboutFragment();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.calculatorFrameLayout, aboutFragment);
                ft.addToBackStack(null);
                ft.commit();
                return true;
            default:
                return true;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
