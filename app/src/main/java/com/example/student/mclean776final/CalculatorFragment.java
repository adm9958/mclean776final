package com.example.student.mclean776final;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CalculatorFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CalculatorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalculatorFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private EditText tempEditText;
    private Button toCelsiusButton;
    private Button toFahrenheitButton;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

            if (tempEditText.getText().toString().isEmpty()) {
                Toast.makeText(getActivity().getBaseContext(),
                        "You did not enter a temperature to convert", Toast.LENGTH_SHORT).show();
                return;
            }
            Button button = (Button) v;
            String s = (String) button.getText();

            if (button.getText().toString().equals("To Celsius")) {
                double temp = Double.valueOf(tempEditText.getText().toString());
                double tempInCelsius = (5.0 / 9.0) * (temp - 32.0);
                conversionDisplayTextView.setText(String.valueOf(temp) + ((char) 0x00B0) + " converted to Celsius is: " +
                        String.valueOf(tempInCelsius) + ((char) 0x00B0) + "C");
            } else {
                double temp = Double.valueOf(tempEditText.getText().toString());
                double tempInFahrenheit = (9.0 / 5.0 * temp) + 32.0;
                conversionDisplayTextView.setText(String.valueOf(temp) + (char) 0x00B0 + " converted to Fahrenheit is: " +
                        String.valueOf(tempInFahrenheit) + (char) 0x00B0 + "F");
            }
        }
    };
    private TextView conversionDisplayTextView;

    public CalculatorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calculator, container, false);
        tempEditText = (EditText) view.findViewById(R.id.tempEditText);
        tempEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)
                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInputFromInputMethod(getView().getWindowToken(), 0);
            }
        });
        toCelsiusButton = (Button) view.findViewById(R.id.toCelsiusButton);
        toFahrenheitButton = (Button) view.findViewById(R.id.toFahrenheitButton);
        toCelsiusButton.setOnClickListener(onClickListener);
        toFahrenheitButton.setOnClickListener(onClickListener);
        conversionDisplayTextView = (TextView) view.findViewById(R.id.conversionDisplayTextView);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
